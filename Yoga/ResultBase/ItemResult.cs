﻿using ResultBase.Interfaces;
using System;

namespace ResultBase
{
    public class ItemResult<T, TS> : IItemResult<T, TS>
        where TS : Enum
    {
        public T Data { get; set; }
        public TS Status { get; set; }
    }
}
