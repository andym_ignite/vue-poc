﻿using ResultBase.Interfaces;
using System;
using System.Collections.Generic;

namespace ResultBase
{
    public class ErrorResult<T, TS> : IErrorResult<T, TS>
        where TS : Enum
    {
        public Dictionary<string, Exception> ErrorCollection { get; set; } = new Dictionary<string, Exception>();
        public TS Status { get; set; }
    }
}
