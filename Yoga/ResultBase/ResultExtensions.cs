﻿using AutoMapper;
using ResultBase.Interfaces;
using System;

namespace ResultBase
{
    public static class ResultExtensions
    {
        public static IErrorResult<T, TS> AddError<T, TS>(this IErrorResult<T, TS> errorResult, Exception exception)
            where TS : Enum
        {
            errorResult.ErrorCollection.Add(Guid.NewGuid().ToString(), exception);
            return errorResult;
        }

        public static IErrorResult<T, TS> AddError<T, TS>(this IErrorResult<T, TS> errorResult, Exception exception, string errorInfo)
            where TS : Enum
        {
            errorResult.ErrorCollection.Add(errorInfo, exception);
            return errorResult;
        }

        public static IErrorResult<T,TS> AddError<T, TS>(this IResult<T, TS> result, Exception exception)
            where TS : Enum
        {
            var errorResult = new ErrorResult<T, TS>
            {
                Status = result.Status
            };

            errorResult.ErrorCollection.Add(Guid.NewGuid().ToString(), exception);
            return errorResult;
        }

        public static IErrorResult<T, TS> AddError<T, TS>(this IResult<T, TS> result, Exception exception, string errorInfo)
            where TS : Enum
        {
            var errorResult = new ErrorResult<T, TS>
            {
                Status = result.Status
            };

            errorResult.ErrorCollection.Add(errorInfo, exception);
            return errorResult;
        }

        public static IResult<TT, TS> ToMappedResult<TF, TT, TS>(this IResult<TF, TS> result)
            where TS : Enum
        {
            return new Result<TT, TS>
            {
                Status = result.Status
            };
        }

        public static IErrorResult<T, TS> ToErrorResult<T, TS>(this IResult<T, TS> result)
            where TS : Enum
        {
            return result as IErrorResult<T, TS>;
        }

        public static IErrorResult<TT, TS> ToMappedErrorResult<TF, TT, TS>(this IResult<TF, TS> result)
            where TS : Enum
        {
            var errorResult = result.ToErrorResult();
            return new ErrorResult<TT, TS>
            {
                Status = errorResult.Status,
                ErrorCollection = errorResult.ErrorCollection
            };
        }

        public static IItemResult<T, TS> ToItemResult<T, TS>(this IResult<T, TS> result)
            where TS : Enum
        {
            return result as IItemResult<T, TS>;
        }

        public static IItemResult<TT, TS> ToMappedItemResult<TF, TT, TS>(this IResult<TF, TS> result, IMapper mapper)
            where TS : Enum
        {
            var itemResult = result.ToItemResult();
            return new ItemResult<TT, TS>
            {
                Status = itemResult.Status,
                Data = mapper.Map<TT>(itemResult.Data)
            };
        }

        public static IPagedResult<T, TS> ToPagedResult<T, TS>(this IResult<T, TS> result)
            where TS : Enum
        {
            return result as IPagedResult<T, TS>;
        }

        public static IPagedResult<TT, TS> ToMappedPagedResult<TF, TT, TS>(this IResult<TF, TS> result, IMapper mapper)
            where TS : Enum
        {
            var pagedResult = result.ToPagedResult();
            return new PagedResult<TT, TS>
            {
                Status = pagedResult.Status,
                TotalResults = pagedResult.TotalResults,
                Page = pagedResult.Page,
                Size = pagedResult.Size,
                Data = mapper.Map<TT>(pagedResult.Data)
            };
        }
    }
}
