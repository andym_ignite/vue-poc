﻿using System.Threading.Tasks;
using Customer;
using Customer.Common.Models.Status;
using Microsoft.AspNetCore.Mvc;
using ResultBase;

namespace Yoga.Controllers
{
    [Route("api/customer")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly IManageCustomer _manageCustomer;
        private readonly IRetrieveCustomer _retrieveCustomer;

        public CustomerController(IManageCustomer manageCustomer, IRetrieveCustomer retrieveCustomer)
        {
            _manageCustomer = manageCustomer;
            _retrieveCustomer = retrieveCustomer;
        }

        // GET api/customer
        [HttpGet]
        public async Task<IActionResult> GetCustomers(int page = 1, int size = 5)
        {
            var result = await _retrieveCustomer.GetCustomersAsync(page, size);
            if (result.Status == CustomerStatus.Success)
            {
                var customers = result.ToPagedResult();
                return Ok(customers);
            }

            return StatusCode(500);
        }

        // GET api/customer/5
        [HttpGet("{customerId}")]
        public async Task<IActionResult> GetCustomerById(int customerId)
        {
            var result = await _retrieveCustomer.GetCustomerByIdAsync(customerId);
            if (result.Status == CustomerStatus.Success)
            {
                var customer = result.ToItemResult();
                return Ok(customer.Data);
            }
            if (result.Status == CustomerStatus.NotFound)
            {
                return NotFound();
            }

            return StatusCode(500);
        }

        // POST api/customer
        [HttpPost]
        public async Task<IActionResult> CreateCustomer([FromBody] Customer.Models.Customer customer)
        {
            var result = await _manageCustomer.CreateCustomerAsync(customer);
            if (result.Status == CustomerStatus.Success)
            {
                var customerResult = result.ToItemResult();
                return Ok(customerResult.Data);
            }

            return StatusCode(500);
        }

        // PUT api/customer
        [HttpPut("{customerId}")]
        public async Task<IActionResult> UpdateCustomer([FromBody] Customer.Models.Customer customer, int customerId)
        {
            if (customer.CustomerId != customerId)
            {
                return BadRequest("CustomerId must match the object you are updating");
            }

            var result = await _manageCustomer.UpdateCustomerAsync(customer);
            if (result.Status == CustomerStatus.Success)
            {
                return Ok();
            }

            return StatusCode(500);
        }

        // DELETE api/cusomter/5
        [HttpDelete("{customerId}")]
        public async Task<IActionResult> Delete(int customerId)
        {
            var result = await _manageCustomer.DeleteCustomerAsync(customerId);
            if (result.Status == CustomerStatus.Success)
            {
                return Ok();
            }

            return StatusCode(500);
        }
    }
}
