﻿using ResponseBase.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;

namespace ResponseBase
{
    public class ErrorResponse<T> : IErrorResponse<T>
    {
        public Dictionary<string, Exception> ErrorCollection
        {
#if DEBUG
            get;
#else
            get { return null; }
#endif
            set;
        }
        public string Message { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}
