﻿using ResponseBase.Interfaces;
using System.Net;

namespace ResponseBase
{
    public class Response<T> : IResponse<T>
    {
        public HttpStatusCode StatusCode { get; set; }
    }
}
