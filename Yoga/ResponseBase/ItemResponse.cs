﻿using ResponseBase.Interfaces;
using System.Net;

namespace ResponseBase
{
    public class ItemResponse<T> : IItemResponse<T>
    {
        public T Data { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}
