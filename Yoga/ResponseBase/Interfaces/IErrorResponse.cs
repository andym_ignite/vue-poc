﻿using System;
using System.Collections.Generic;

namespace ResponseBase.Interfaces
{
    public interface IErrorResponse<T> : IResponse<T>
    {
        // Only output in debug mode
        Dictionary<string, Exception> ErrorCollection { get; set; }
        string Message { get; set; }
    }
}
