﻿using ResponseBase.Interfaces;
using System.Net;

namespace ResponseBase
{
    public class PagedResponseS<T> : IPagedResponse<T>
    {
        public int TotalResults { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
        public T Data { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}
