﻿using Microsoft.EntityFrameworkCore;

namespace Customer.Data
{
    public class CustomerDataContext : DbContext
    {
        public CustomerDataContext(DbContextOptions<CustomerDataContext> options)
            : base(options)
        {
        }

        internal DbSet<DataModels.Customer> Customers { get; set; }
        internal DbSet<DataModels.Address> Addresses { get; set; }
    }
}
