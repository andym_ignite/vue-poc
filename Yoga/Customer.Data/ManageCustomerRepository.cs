﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Customer.Common.Models.Status;
using Customer.Data.Models;
using Microsoft.Extensions.Logging;
using ResultBase;
using ResultBase.Interfaces;

namespace Customer.Data
{
    public class ManageCustomerRepository : IManageCustomerRepository
    {
        private readonly CustomerDataContext _customerDataContext;
        private readonly ILogger<ManageCustomerRepository> _logger;
        private readonly IMapper _mapper;

        public ManageCustomerRepository(CustomerDataContext customerDataContext, ILogger<ManageCustomerRepository> logger, IMapper mapper)
        {
            _customerDataContext = customerDataContext;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<IResult<Address, AddressStatus>> CreateCustomerAddressAsync(Address address)
        {
            try
            {
                await _customerDataContext.AddAsync(_mapper.Map<DataModels.Address>(address));
                await _customerDataContext.SaveChangesAsync();

                _logger.LogDebug("Successfully created address for customer: {CustomerId}", address.CustomerId);
                return new Result<Address, AddressStatus>
                {
                    Status = AddressStatus.Success
                };
            }
            catch(Exception ex)
            {
                var errorResult = new ErrorResult<Address, AddressStatus>
                {
                    Status = AddressStatus.FailedToCreate
                };

                _logger.LogError(ex, "Failed to create address for customer: {CustomerId}", address.CustomerId);
                errorResult.AddError(ex, AddressStatus.FailedToCreate.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<Models.Customer, CustomerStatus>> CreateCustomerAsync(Models.Customer customer)
        {
            try
            {
                var customerData = _mapper.Map<DataModels.Customer>(customer);
                await _customerDataContext.AddAsync(customerData);
                await _customerDataContext.SaveChangesAsync();

                _logger.LogDebug("Successfully created customer: {Name}", $"{customer.Forename} {customer.Surname}");
                return new ItemResult<Models.Customer, CustomerStatus>
                {
                    Data = _mapper.Map<Models.Customer>(customerData),
                    Status = CustomerStatus.Success
                };
            }
            catch (Exception ex)
            {
                var errorResult = new ErrorResult<Models.Customer, CustomerStatus>
                {
                    Status = CustomerStatus.FailedToCreate
                };

                _logger.LogError(ex, "Failed to create customer: {Name}", $"{customer.Forename} {customer.Surname}");
                errorResult.AddError(ex, CustomerStatus.FailedToCreate.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<Address, AddressStatus>> DeleteCustomerAddressAsync(int addressId)
        {
            try
            {
                var currentAddress = _customerDataContext.Addresses
                    .Where(a => a.AddressId == addressId)
                    .SingleOrDefault();

                if (currentAddress == null)
                {
                    _logger.LogError("Could not find an address with the Id: {AddressId} to delete", addressId);
                    return new Result<Address, AddressStatus>
                    {
                        Status = AddressStatus.NotFound
                    };
                }

                _customerDataContext.Addresses.Remove(currentAddress);
                await _customerDataContext.SaveChangesAsync();

                _logger.LogDebug("Successfully deleted address: {AddressId} for customer: {CustomerId}", currentAddress.AddressId, currentAddress.CustomerId);
                return new Result<Address, AddressStatus>
                {
                    Status = AddressStatus.Success
                };
            }
            catch (Exception ex)
            {
                var errorResult = new ErrorResult<Address, AddressStatus>
                {
                    Status = AddressStatus.FailedToDelete
                };

                _logger.LogError(ex, "Failed to delete address: {AddressId}", addressId);
                errorResult.AddError(ex, AddressStatus.FailedToDelete.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<Address, AddressStatus>> DeleteCustomerAddressesAsync(int customerId)
        {
            try
            {
                var currentAddresses = _customerDataContext.Addresses
                    .Where(a => a.CustomerId == customerId);

                if (!currentAddresses.Any())
                {
                    _logger.LogError("Could not find any addresses for customer: {CustomerId}", customerId);
                    return new Result<Address, AddressStatus>
                    {
                        Status = AddressStatus.NotFound
                    };
                }

                _customerDataContext.Addresses.RemoveRange(currentAddresses);
                await _customerDataContext.SaveChangesAsync();

                _logger.LogDebug("Successfully deleted {Count} addresses for customer: {CustomerId}", currentAddresses.Count(), customerId);
                return new Result<Address, AddressStatus>
                {
                    Status = AddressStatus.Success
                };
            }
            catch (Exception ex)
            {
                var errorResult = new ErrorResult<Address, AddressStatus>
                {
                    Status = AddressStatus.FailedToDelete
                };

                _logger.LogError(ex, "Failed to delete any addresses for customer: {CustomerId}", customerId);
                errorResult.AddError(ex, AddressStatus.FailedToDelete.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<Models.Customer, CustomerStatus>> DeleteCustomerAsync(int customerId)
        {
            try
            {
                var currentCustomer = _customerDataContext.Customers
                    .Where(a => a.CustomerId == customerId)
                    .SingleOrDefault();

                if (currentCustomer == null)
                {
                    _logger.LogError("Could not find a customer with Id: {CustomerId} to delete", customerId);
                    return new Result<Models.Customer, CustomerStatus>
                    {
                        Status = CustomerStatus.NotFound
                    };
                }

                _customerDataContext.Customers.Remove(currentCustomer);
                await _customerDataContext.SaveChangesAsync();

                _logger.LogDebug("Successfully deleted customer: {CustomerId}", currentCustomer.CustomerId);
                return new Result<Models.Customer, CustomerStatus>
                {
                    Status = CustomerStatus.Success
                };
            }
            catch (Exception ex)
            {
                var errorResult = new ErrorResult<Models.Customer, CustomerStatus>
                {
                    Status = CustomerStatus.FailedToDelete
                };

                _logger.LogError(ex, "Failed to delete customer: {CustomerId}", customerId);
                errorResult.AddError(ex, CustomerStatus.FailedToDelete.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<Address, AddressStatus>> UpdateCustomerAddressAsync(Address address)
        {
            try
            {
                var currentAddress = _customerDataContext.Addresses
                    .Where(a => a.AddressId == address.AddressId)
                    .SingleOrDefault();

                if (currentAddress == null)
                {
                    _logger.LogError("Could not find address: {AddressId} to update address for customer: {CustomerId}", address.AddressId, address.CustomerId);
                    return new Result<Address, AddressStatus>
                    {
                        Status = AddressStatus.NotFound
                    };
                }

                _customerDataContext.Entry(currentAddress).CurrentValues.SetValues(address);
                await _customerDataContext.SaveChangesAsync();

                _logger.LogDebug("Successfully updated address: {AddressId} for customer: {CustomerId}", address.AddressId, address.CustomerId);
                return new Result<Address, AddressStatus>
                {
                    Status = AddressStatus.Success
                };
            }
            catch (Exception ex)
            {
                var errorResult = new ErrorResult<Address, AddressStatus>
                {
                    Status = AddressStatus.FailedToUpdate
                };

                _logger.LogError(ex, "Failed to update address for customer: {CustomerId}", address.CustomerId);
                errorResult.AddError(ex, AddressStatus.FailedToUpdate.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<Models.Customer, CustomerStatus>> UpdateCustomerAsync(Models.Customer customer)
        {
            try
            {
                var currentCustomer = _customerDataContext.Customers
                    .Where(a => a.CustomerId == customer.CustomerId)
                    .SingleOrDefault();

                if (currentCustomer == null)
                {
                    _logger.LogError("Could not find customer: {CustomerId}", customer.CustomerId);
                    return new Result<Models.Customer, CustomerStatus>
                    {
                        Status = CustomerStatus.NotFound
                    };
                }

                _customerDataContext.Entry(currentCustomer).CurrentValues.SetValues(customer);
                await _customerDataContext.SaveChangesAsync();

                _logger.LogDebug("Successfully updated customer: {CustomerId}", customer.CustomerId);
                return new Result<Models.Customer, CustomerStatus>
                {
                    Status = CustomerStatus.Success
                };
            }
            catch (Exception ex)
            {
                var errorResult = new ErrorResult<Models.Customer, CustomerStatus>
                {
                    Status = CustomerStatus.FailedToUpdate
                };

                _logger.LogError(ex, "Failed to update customer: {CustomerId}", customer.CustomerId);
                errorResult.AddError(ex, CustomerStatus.FailedToUpdate.ToString());
                return errorResult;
            }
        }
    }
}
