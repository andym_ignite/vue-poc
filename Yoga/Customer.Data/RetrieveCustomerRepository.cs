﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Customer.Common.Models.Status;
using Customer.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ResultBase;
using ResultBase.Interfaces;

namespace Customer.Data
{
    public class RetrieveCustomerRepository : IRetrieveCustomerRepository
    {
        private readonly CustomerDataContext _customerDataContext;
        private readonly ILogger<RetrieveCustomerRepository> _logger;
        private readonly IMapper _mapper;

        public RetrieveCustomerRepository(CustomerDataContext customerDataContext, ILogger<RetrieveCustomerRepository> logger, IMapper mapper)
        {
            _customerDataContext = customerDataContext;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<IResult<Address, AddressStatus>> GetAddressByIdAsync(int addressId)
        {
            try
            {
                var address = await _customerDataContext.Addresses.FindAsync(addressId);
                if (address == null)
                {
                    _logger.LogError("Could not find address: {AddressId}", addressId);
                    return new Result<Address, AddressStatus>
                    {
                        Status = AddressStatus.NotFound
                    };
                }

                _logger.LogDebug("Successfully retrieved address: {AddressId} for customer: {CustomerId}", address.AddressId, address.CustomerId);
                return new ItemResult<Address, AddressStatus>
                {
                    Status = AddressStatus.Success,
                    Data = _mapper.Map<Address>(address)
                };
            }
            catch (Exception ex)
            {
                var errorResult = new ErrorResult<Address, AddressStatus>
                {
                    Status = AddressStatus.FailedToRetrieve
                };

                _logger.LogError(ex, "Failed to retrieve address: {AddressId}", addressId);
                errorResult.AddError(ex, AddressStatus.FailedToRetrieve.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<IEnumerable<Address>, AddressStatus>> GetAddressesByCustomerIdAsync(int customerId, int page = 1, int size = 5)
        {
            try
            {
                var addresses = await _customerDataContext.Addresses
                    .Where(a => a.CustomerId == customerId)
                    .ToListAsync();

                _logger.LogDebug("Successfully retrieved {Count} for customer: {CustomerId}", addresses.Count, customerId);
                return new PagedResult<IEnumerable<Address>, AddressStatus>
                {
                    Status = AddressStatus.Success,
                    TotalResults = addresses.Count,
                    Page = page,
                    Size = size,
                    Data = _mapper.Map<IEnumerable<Address>>(addresses.Page(page, size))
                };
            }
            catch (Exception ex)
            {
                var errorResult = new ErrorResult<IEnumerable<Address>, AddressStatus>
                {
                    Status = AddressStatus.FailedToRetrieve
                };

                _logger.LogError(ex, "Failed to retrieve customer: {CustomerId}", customerId);
                errorResult.AddError(ex, AddressStatus.FailedToRetrieve.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<Models.Customer, CustomerStatus>> GetCustomerByIdAsync(int customerId)
        {
            try
            {
                var customer = await _customerDataContext.Customers.FindAsync(customerId);
                if (customer == null)
                {
                    _logger.LogError("Could not find customer: {CustomerId}", customerId);
                    return new Result<Models.Customer, CustomerStatus>
                    {
                        Status = CustomerStatus.NotFound
                    };
                }

                _logger.LogDebug("Successfully retrieved customer: {CustomerId}", customer.CustomerId);
                return new ItemResult<Models.Customer, CustomerStatus>
                {
                    Status = CustomerStatus.Success,
                    Data = _mapper.Map<Models.Customer>(customer)
                };
            }
            catch (Exception ex)
            {
                var errorResult = new ErrorResult<Models.Customer, CustomerStatus>
                {
                    Status = CustomerStatus.FailedToRetrieve
                };

                _logger.LogError(ex, "Failed to retrieve customer: {CustomerId}", customerId);
                errorResult.AddError(ex, CustomerStatus.FailedToRetrieve.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<IEnumerable<Models.Customer>, CustomerStatus>> GetCustomersAsync(int page, int size)
        {
            try
            {
                var customers = await _customerDataContext.Customers.ToListAsync();

                _logger.LogDebug("Successfully retrieved {Count} customers", customers.Count);
                return new PagedResult<IEnumerable<Models.Customer>, CustomerStatus>
                {
                    Status = CustomerStatus.Success,
                    TotalResults = customers.Count,
                    Page = page,
                    Size = size,
                    Data = _mapper.Map<IEnumerable<Models.Customer>>(customers.Page(page, size))
                };
            }
            catch (Exception ex)
            {
                var errorResult = new ErrorResult<IEnumerable<Models.Customer>, CustomerStatus>
                {
                    Status = CustomerStatus.FailedToRetrieve
                };

                _logger.LogError(ex, "Failed to retrieve any customers");
                errorResult.AddError(ex, CustomerStatus.FailedToRetrieve.ToString());
                return errorResult;
            }
        }
    }
}
