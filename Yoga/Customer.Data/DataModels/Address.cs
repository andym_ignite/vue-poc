﻿using Customer.Common.Models.Status;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Customer.Data.DataModels
{
    [Table("ADDRESS")]
    internal class Address
    {
        [Key]
        [Column("Id")]
        public int AddressId { get; set; }
        [ForeignKey("CustomerId")]
        public int CustomerId { get; set; }
        [Column("Line_1")]
        public string Line1 { get; set; }
        [Column("Line_2")]
        public string Line2 { get; set; }
        [Column("Line_3")]
        public string Line3 { get; set; }
        [Column("Town")]
        public string Town { get; set; }
        [Column("County")]
        public string County { get; set; }
        [Column("Country")]
        public string Country { get; set; }
        [Column("PostCode")]
        public string Postcode { get; set; }
        [Column("Type")]
        public AddressType AddressType { get; set; }
    }
}
