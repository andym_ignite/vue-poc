﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Customer.Data.DataModels
{
    [Table("CUSTOMER")]
    internal class Customer
    {
        [Key]
        [Column("Id")]
        public int CustomerId { get; set; }
        [Column("Title")]
        public string Title { get; set; }
        [Column("FirstName")]
        public string Forename { get; set; }
        [Column("LastName")]
        public string Surname { get; set; }
        [Column("DOB")]
        public DateTime DateOfBirth { get; set; }
        [Column("Gender")]
        public string Gender { get; set; }
        [Column("Email")]
        public string Email { get; set; }
    }
}
