﻿using AutoMapper;

namespace Customer.Data.Config
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<DataModels.Address, Models.Address>().ReverseMap();
            CreateMap<DataModels.Customer, Models.Customer>().ReverseMap();
        }
    }
}
