﻿using System.Collections.Generic;
using System.Linq;

namespace Customer.Data
{
    public static class PageExtensions
    {
        public static IEnumerable<T> Page<T>(this IEnumerable<T> results, int page, int size)
        {
            return results.Skip(page - 1 * size).Take(size);
        }
    }
}
