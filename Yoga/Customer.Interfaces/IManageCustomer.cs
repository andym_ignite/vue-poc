﻿using Customer.Common.Models.Status;
using ResultBase.Interfaces;
using System.Threading.Tasks;

namespace Customer
{
    public interface IManageCustomer
    {
        Task<IResult<Models.Customer, CustomerStatus>> CreateCustomerAsync(Models.Customer customer);
        Task<IResult<Models.Address, AddressStatus>> CreateCustomerAddressAsync(Models.Address address);
        Task<IResult<Models.Customer, CustomerStatus>> UpdateCustomerAsync(Models.Customer customer);
        Task<IResult<Models.Address, AddressStatus>> UpdateCustomerAddressAsync(Models.Address address);
        Task<IResult<Models.Customer, CustomerStatus>> DeleteCustomerAsync(int customerId);
        Task<IResult<Models.Address, AddressStatus>> DeleteCustomerAddressAsync(int addressId);
        Task<IResult<Models.Address, AddressStatus>> DeleteCustomerAddressesAsync(int customerId);
    }
}
