﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Customer.Common.Models.Status;
using Customer.Data;
using Customer.Models;
using Microsoft.Extensions.Logging;
using ResultBase;
using ResultBase.Interfaces;

namespace Customer
{
    public class ManageCustomer : IManageCustomer
    {
        private readonly IManageCustomerRepository _manageCustomerRepository;
        private readonly ILogger<ManageCustomer> _logger;
        private readonly IMapper _mapper;

        public ManageCustomer(IManageCustomerRepository manageCustomerRepository, ILogger<ManageCustomer> logger, IMapper mapper)
        {
            _manageCustomerRepository = manageCustomerRepository;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<IResult<Address, AddressStatus>> CreateCustomerAddressAsync(Address address)
        {
            try
            {
                var result = await _manageCustomerRepository.UpdateCustomerAddressAsync(_mapper.Map<Data.Models.Address>(address));
                if (result.Status == AddressStatus.Success)
                {
                    _logger.LogDebug("Successfully created address for customer: {CustomerId}", address.CustomerId);
                    return result.ToMappedResult<Data.Models.Address, Address, AddressStatus>();
                }

                _logger.LogError("Failed to create address for customer: {CustomerId}", address.CustomerId);
                return result.ToMappedErrorResult<Data.Models.Address, Address, AddressStatus>();
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception occurred on attempt to create address for customer: {CustomerId}", address.CustomerId);
                var errorResult = new ErrorResult<Address, AddressStatus>
                {
                    Status = AddressStatus.FailedToCreate
                };

                errorResult.AddError(ex, AddressStatus.FailedToCreate.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<Models.Customer, CustomerStatus>> CreateCustomerAsync(Models.Customer customer)
        {
            try
            {
                var result = await _manageCustomerRepository.CreateCustomerAsync(_mapper.Map<Data.Models.Customer>(customer));
                if (result.Status == CustomerStatus.Success)
                {
                    _logger.LogDebug("Successfully created customer: {Name}", $"{customer.Forename} {customer.Surname}");
                    return result.ToMappedItemResult<Data.Models.Customer, Models.Customer, CustomerStatus>(_mapper);
                }

                _logger.LogError("Failed to create customer: {Name}", $"{customer.Forename} {customer.Surname}");
                return result.ToMappedErrorResult<Data.Models.Customer, Models.Customer, CustomerStatus>();
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception occurred on attempt to create customer: {Name}", $"{customer.Forename} {customer.Surname}");
                var errorResult = new ErrorResult<Models.Customer, CustomerStatus>
                {
                    Status = CustomerStatus.FailedToCreate
                };

                errorResult.AddError(ex, CustomerStatus.FailedToCreate.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<Address, AddressStatus>> DeleteCustomerAddressAsync(int addressId)
        {
            try
            {
                var result = await _manageCustomerRepository.DeleteCustomerAddressAsync(addressId);
                if (result.Status == AddressStatus.Success)
                {
                    _logger.LogDebug("Successfully deleted address: {AddressId}", addressId);
                    return result.ToMappedResult<Data.Models.Address, Address, AddressStatus>();
                }

                _logger.LogError("Failed to delete address: {AddressId}", addressId);
                return result.ToMappedErrorResult<Data.Models.Address, Address, AddressStatus>();
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception occurred on attempt to delete address: {AddressId}", addressId);
                var errorResult = new ErrorResult<Address, AddressStatus>
                {
                    Status = AddressStatus.FailedToDelete
                };

                errorResult.AddError(ex, AddressStatus.FailedToDelete.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<Address, AddressStatus>> DeleteCustomerAddressesAsync(int customerId)
        {
            try
            {
                var result = await _manageCustomerRepository.DeleteCustomerAddressesAsync(customerId);
                if (result.Status == AddressStatus.Success)
                {
                    _logger.LogDebug("Successfully deleted addresses for customer: {CustomerId}", customerId);
                    return result.ToMappedResult<Data.Models.Address, Address, AddressStatus>();
                }

                _logger.LogError("Failed to delete any addresses for customer: {CustomerId}", customerId);
                return result.ToMappedErrorResult<Data.Models.Address, Address, AddressStatus>();
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception occurred on attempt to delete addresses for customer: {CustomerId}", customerId);
                var errorResult = new ErrorResult<Address, AddressStatus>
                {
                    Status = AddressStatus.FailedToCreate
                };

                errorResult.AddError(ex, AddressStatus.FailedToCreate.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<Models.Customer, CustomerStatus>> DeleteCustomerAsync(int customerId)
        {
            try
            {
                var result = await _manageCustomerRepository.DeleteCustomerAsync(customerId);
                if (result.Status == CustomerStatus.Success)
                {
                    _logger.LogDebug("Successfully deleted customer: {CustomerId}", customerId);
                    return result.ToMappedResult<Data.Models.Customer, Models.Customer, CustomerStatus>();
                }

                _logger.LogError("Failed to delete customer: {CustomerId}", customerId);
                return result.ToMappedErrorResult<Data.Models.Customer, Models.Customer, CustomerStatus>();
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception occurred on attempt to delete customer: {CustomerId}", customerId);
                var errorResult = new ErrorResult<Models.Customer, CustomerStatus>
                {
                    Status = CustomerStatus.FailedToDelete
                };

                errorResult.AddError(ex, CustomerStatus.FailedToDelete.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<Address, AddressStatus>> UpdateCustomerAddressAsync(Address address)
        {
            try
            {
                var result = await _manageCustomerRepository.UpdateCustomerAddressAsync(_mapper.Map<Data.Models.Address>(address));
                if (result.Status == AddressStatus.Success)
                {
                    _logger.LogDebug("Successfully updated address for customer: {CustomerId}", address.CustomerId);
                    return result.ToMappedResult<Data.Models.Address, Address, AddressStatus>();
                }

                _logger.LogError("Failed to update address for customer: {CustomerId}", address.CustomerId);
                return result.ToMappedErrorResult<Data.Models.Address, Address, AddressStatus>();
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception occurred on attempt to update address for customer: {CustomerId}", address.CustomerId);
                var errorResult = new ErrorResult<Address, AddressStatus>
                {
                    Status = AddressStatus.FailedToUpdate
                };

                errorResult.AddError(ex, AddressStatus.FailedToUpdate.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<Models.Customer, CustomerStatus>> UpdateCustomerAsync(Models.Customer customer)
        {
            try
            {
                var result = await _manageCustomerRepository.UpdateCustomerAsync(_mapper.Map<Data.Models.Customer>(customer));
                if (result.Status == CustomerStatus.Success)
                {
                    _logger.LogDebug("Successfully updated customer: {CustomerId}", customer.CustomerId);
                    return result.ToMappedResult<Data.Models.Customer, Models.Customer, CustomerStatus>();
                }

                _logger.LogError("Failed to update customer: {CustomerId}", customer.CustomerId);
                return result.ToMappedErrorResult<Data.Models.Customer, Models.Customer, CustomerStatus>();
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception occurred on attempt to update customer: {CustomerId}", customer.CustomerId);
                var errorResult = new ErrorResult<Models.Customer, CustomerStatus>
                {
                    Status = CustomerStatus.FailedToUpdate
                };

                errorResult.AddError(ex, CustomerStatus.FailedToUpdate.ToString());
                return errorResult;
            }
        }
    }
}
