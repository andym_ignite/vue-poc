﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Customer.Common.Models.Status;
using Customer.Data;
using Customer.Models;
using Microsoft.Extensions.Logging;
using ResultBase;
using ResultBase.Interfaces;

namespace Customer
{
    public class RetrieveCustomer : IRetrieveCustomer
    {
        private readonly IRetrieveCustomerRepository _retrieveCustomerRepository;
        private readonly ILogger<RetrieveCustomer> _logger;
        private readonly IMapper _mapper;

        public RetrieveCustomer(IRetrieveCustomerRepository retrieveCustomerRepository, ILogger<RetrieveCustomer> logger, IMapper mapper)
        {
            _retrieveCustomerRepository = retrieveCustomerRepository;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<IResult<Address, AddressStatus>> GetAddressByIdAsync(int addressId)
        {
            try
            {
                var result = await _retrieveCustomerRepository.GetAddressByIdAsync(addressId);
                if (result.Status == AddressStatus.Success)
                {
                    _logger.LogDebug("Successfully retrieved address: {AddressId}", addressId);
                    return result.ToMappedItemResult<Data.Models.Address, Address, AddressStatus>(_mapper);
                }

                _logger.LogError("Failed to retrieve address: {AddressId}", addressId);
                return result.ToMappedErrorResult<Data.Models.Address, Address, AddressStatus>();
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception occurred on attempt to retrieve address: {AddressId}", addressId);
                var errorResult = new ErrorResult<Models.Address, AddressStatus>
                {
                    Status = AddressStatus.FailedToRetrieve
                };

                errorResult.AddError(ex, AddressStatus.FailedToRetrieve.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<IEnumerable<Address>, AddressStatus>> GetAddressesByCustomerIdAsync(int customerId, int page = 1, int size = 5)
        {
            try
            {
                var result = await _retrieveCustomerRepository.GetAddressesByCustomerIdAsync(customerId, page, size);
                if (result.Status == AddressStatus.Success)
                {
                    _logger.LogDebug("Successfully retrieved addresses for customer: {CustomId}", customerId);
                    return result.ToMappedPagedResult<IEnumerable<Data.Models.Address>, IEnumerable<Address>, AddressStatus>(_mapper);
                }

                _logger.LogError("Failed to retrieve address: {AddressId}", customerId);
                return result.ToMappedErrorResult<IEnumerable<Data.Models.Address>, IEnumerable<Address>, AddressStatus>();
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception occurred on attempt to retrieve address: {AddressId}", customerId);
                var errorResult = new ErrorResult<IEnumerable<Address>, AddressStatus>
                {
                    Status = AddressStatus.FailedToRetrieve
                };

                errorResult.AddError(ex, AddressStatus.FailedToRetrieve.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<Models.Customer, CustomerStatus>> GetCustomerByIdAsync(int customerId)
        {
            try
            {
                var result = await _retrieveCustomerRepository.GetCustomerByIdAsync(customerId);
                if (result.Status == CustomerStatus.Success)
                {
                    _logger.LogDebug("Successfully retrieved customer: {CustomerId}", customerId);
                    return result.ToMappedItemResult<Data.Models.Customer, Models.Customer, CustomerStatus>(_mapper);
                }

                if (result.Status == CustomerStatus.NotFound)
                {
                    _logger.LogError("Failed to retrieve customer: {CustomerId}", customerId);
                    return result.ToMappedResult<Data.Models.Customer, Models.Customer, CustomerStatus>();
                }

                _logger.LogError("Failed to retrieve customer: {CustomerId}", customerId);
                return result.ToMappedErrorResult<Data.Models.Customer, Models.Customer, CustomerStatus>();
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception occurred on attempt to retrieve customer: {CustomerId}", customerId);
                var errorResult = new ErrorResult<Models.Customer, CustomerStatus>
                {
                    Status = CustomerStatus.FailedToRetrieve
                };

                errorResult.AddError(ex, CustomerStatus.FailedToRetrieve.ToString());
                return errorResult;
            }
        }

        public async Task<IResult<IEnumerable<Models.Customer>, CustomerStatus>> GetCustomersAsync(int page = 1, int size = 5)
        {
            try
            {
                var result = await _retrieveCustomerRepository.GetCustomersAsync(page, size);
                if (result.Status == CustomerStatus.Success)
                {
                    _logger.LogDebug("Successfully retrieved customers");
                    return result.ToMappedPagedResult<IEnumerable<Data.Models.Customer>, IEnumerable<Models.Customer>, CustomerStatus>(_mapper);
                }

                _logger.LogError("Failed to retrieve customers");
                return result.ToMappedErrorResult<IEnumerable<Data.Models.Customer>, IEnumerable<Models.Customer>, CustomerStatus>();
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception occurred on attempt to retrieve customers");
                var errorResult = new ErrorResult<IEnumerable<Models.Customer>, CustomerStatus>
                {
                    Status = CustomerStatus.FailedToRetrieve
                };

                errorResult.AddError(ex, CustomerStatus.FailedToRetrieve.ToString());
                return errorResult;
            }
        }
    }
}
