﻿using AutoMapper;

namespace Customer.Config
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<Data.Models.Address, Models.Address>().ReverseMap();
            CreateMap<Data.Models.Customer, Models.Customer>().ReverseMap();
        }
    }
}
