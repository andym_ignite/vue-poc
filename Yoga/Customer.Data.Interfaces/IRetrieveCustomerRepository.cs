﻿using Customer.Common.Models.Status;
using ResultBase.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Customer.Data
{
    public interface IRetrieveCustomerRepository
    {
        Task<IResult<Models.Customer, CustomerStatus>> GetCustomerByIdAsync(int customerId);
        Task<IResult<IEnumerable<Models.Customer>, CustomerStatus>> GetCustomersAsync(int page = 1, int size = 5);
        Task<IResult<Models.Address, AddressStatus>> GetAddressByIdAsync(int addressId);
        Task<IResult<IEnumerable<Models.Address>, AddressStatus>> GetAddressesByCustomerIdAsync(int customerId, int page = 1, int size = 5);
    }
}
