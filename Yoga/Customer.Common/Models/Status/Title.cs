﻿namespace Customer.Common.Models.Status
{
    public enum Title
    {
        Mr,
        Miss,
        Mrs,
        Ms,
        Mx,
        Dr,
        Prof,
        Sir
    }
}
