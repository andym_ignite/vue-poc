﻿namespace Customer.Common.Models.Status
{
    public enum AddressStatus
    {
        Success,
        NotFound,
        FailedToDelete,
        FailedToCreate,
        FailedToUpdate,
        FailedToRetrieve
    }
}
