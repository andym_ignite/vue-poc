﻿namespace Customer.Common.Models.Status
{
    public enum CustomerStatus
    {
        Success,
        NotFound,
        FailedToCreate,
        FailedToUpdate,
        FailedToDelete,
        FailedToRetrieve
    }
}
