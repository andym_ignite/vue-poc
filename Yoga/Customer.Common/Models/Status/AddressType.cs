﻿namespace Customer.Common.Models.Status
{
    public enum AddressType
    {
        Insurable = 1,
        Billing = 2,
        Correspondence = 3,
        Business = 4
    }
}