﻿namespace Customer.Common.Models.Status
{
    public enum Gender
    {
        Male,
        Female,
        NotSpecified
    }
}
