import axios from './axios'

const actions = {
  loadCustomers ({ commit }) {
    axios
      .get('/api/customer')
      .then(response => response.data)
      .then(pagedResponse => {
        commit('setCustomers', pagedResponse.data
        )
      },
      (err) => {
        console.log(err)
      })
  },
  getCustomer ({ commit }, customerId) {
    axios
      .get(`/api/customer/${customerId}`)
      .then(response => {
        commit('setCustomer', response.data
        )
      },
      (err) => {
        console.log(err)
      })
  },
  createCustomer ({commit}, customer) {
    // Call out to REST service
    axios
      .post('/api/customer', customer)
      .then(response => response.data)
      .then(customerData => {
        commit('createCustomer', customerData)
      },
      (err) => {
        console.log(err)
      })
  },
  updateCustomer ({commit}, customer) {
    console.log(customer)
    // Call out to REST service
    axios
      .put('/api/customer/' + customer.customerId, customer)
      .then(response => response.data)
      .then(customerData => {
        commit('updateCustomer', customerData)
      },
      (err) => {
        console.log(err)
      })
  },
  deleteCustomer ({commit}, customerId) {
    // Call out to REST service
    axios
      .delete(`/api/customer/${customerId}`, customerId)
      .then(response => {
        commit('deleteCustomer', customerId)
      },
      (err) => {
        console.log(err)
      })
  }
}

export default actions
