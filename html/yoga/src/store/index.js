import Vue from 'vue'
import Vuex from 'vuex'
import VueAxios from 'vue-axios'
import axios from 'axios'
import mutations from './mutations'
import actions from './actions'
import getters from './getters'

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export const store = new Vuex.Store({
  state: {
    customers: [],
    customer: {},
    loadedPoses: [
      {
        imageUrl: 'https://c1.staticflickr.com/9/8071/29438001551_8c902c8f14_b.jpg',
        id: '1',
        title: 'Yoga Beach'
      },
      {
        imageUrl: 'https://c1.staticflickr.com/4/3954/32741910104_bd0c642659_b.jpg',
        id: '2',
        title: 'Yoga Sunset'
      },
      {
        imageUrl: 'https://c1.staticflickr.com/1/766/20934593035_abb413fecc_b.jpg',
        id: '3',
        title: 'Yoga Horizon'
      }
    ]
  },
  mutations,
  actions,
  getters
})
