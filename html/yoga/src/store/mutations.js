const mutations = {
  setCustomers (state, customers) {
    state.customers = customers
  },
  setCustomer (state, customer) {
    state.customer = customer
  },
  createCustomer (state, customer) {
    state.customers.push(customer)
  },
  updateCustomer (state, customer) {
    let itemToUpdate = state.customers.findIndex(i => i.customerId === customer.customerId)
    Object.assign(itemToUpdate, customer)
  },
  deleteCustomer (state, customerId) {
    state.customers.splice(state.customers.findIndex(i => i.customerId === customerId), 1)
  }
}

export default mutations
