const getters = {
  customers (state) {
    return state.customers.sort((customerA, customerB) => {
      return customerA.customerId > customerB.customerId
    })
  },
  customer (state) {
    return (customerId) => {
      return state.customers.find((customer) => {
        return customer.customerId === customerId
      })
    }
  },
  loadedPoses (state) {
    return state.loadedPoses.sort((poseA, poseB) => {
      return poseA.id > poseB.id
    })
  },
  loadedPose (state) {
    return (poseId) => {
      return state.loadedPoses.find((pose) => {
        return pose.customerId === poseId
      })
    }
  },
  featuredPoses (state, getters) {
    return getters.loadedPoses.slice(0, 5)
  },
  randomYogaPose (state, getters) {
    return getters.loadedPoses[Math.floor(Math.random() * getters.loadedPoses.length)]
  }
}

export default getters
