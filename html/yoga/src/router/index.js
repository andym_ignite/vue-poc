import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Customers from '@/components/Customer/Customers'
import CreateCustomer from '@/components/Customer/CreateCustomer'
import EditCustomer from '@/components/Customer/EditCustomer'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/customer',
      name: 'Customers',
      component: Customers
    },
    {
      path: '/customer/create',
      name: 'CreateCustomer',
      component: CreateCustomer
    },
    {
      path: '/customer/edit/:id',
      name: 'EditCustomer',
      props: true,
      component: EditCustomer
    }
  ],
  mode: 'history'
})
